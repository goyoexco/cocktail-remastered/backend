# Cocktail Remastered - Backend

- [Cocktail Remastered - Backend](#cocktail-remastered---backend)
  - [1. Introduction](#1-introduction)
  - [2. Development](#2-development)
    - [2.1. Prerequisites](#21-prerequisites)
    - [2.2. Launch in development mode](#22-launch-in-development-mode)

## 1. Introduction

This code base is in relation with the other parts of the Cocktail Remastered project.
For more convenience here is the recommended order to install the folders :

- Utils
- Backend
- Front

## 2. Development

### 2.1. Prerequisites

The following software must be installed:

- [Docker](https://docs.docker.com/engine/install/)
- [Node LTS 16+](https://nodejs.org/en/download/)

Optional :

- [Postman](https://www.postman.com/) (Recommended as there is no Open API on the project to test your endpoints. See **Cocktail App.postman_collection.json**)

### 2.2. Launch in development mode

To start the backend component :

- `backend`:

  ```bash
  yarn install
  yarn start
  ```

Nodemon is installed in case you might want to hot reload the backend while developping.

```bash
nodemon server.js
```

If the installation is correct you should see :

```bash
Server is running on port 8080.
```
