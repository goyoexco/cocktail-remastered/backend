module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
  },
  extends: ['airbnb-base'],
  parserOptions: {
    ecmaVersion: 'latest',
  },
  rules: {
    'linebreak-style': 0,
    'no-plusplus': 0,
    'consistent-return': 0,
    camelcase: 0,
    semi: ['error', 'never'],
    'import/no-dynamic-require': 0,
    'global-require': 0,
    'implicit-arrow-linebreak': ['error', 'below'],
  },
}
