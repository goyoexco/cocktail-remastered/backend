const Sequelize = require('sequelize')

/**
 * Actions summary:
 *
 * createTable "cocktails", deps: []
 * createTable "ingredients", deps: []
 * createTable "roles", deps: []
 * createTable "types", deps: []
 * createTable "users", deps: []
 * createTable "cocktail_ingredients", deps: [cocktails, ingredients]
 * createTable "user_roles", deps: ["public"."roles", "public"."users"]
 * createTable "recipe", deps: [cocktails]
 * addIndex "cocktail_ingredients_indexes" to table "cocktail_ingredients"
 * addIndex "cocktails_indexes" to table "cocktails"
 * addIndex "ingredients_indexes" to table "ingredients"
 * addIndex "recipe_indexes" to table "recipe"
 * addIndex "roles_indexes" to table "roles"
 * addIndex "types_indexes" to table "types"
 * addIndex "users_indexes" to table "users"
 *
 * */

const info = {
  revision: 1,
  name: 'noname',
  created: '2022-02-19T10:02:25.976Z',
  comment: '',
}

const migrationCommands = function (transaction) {
  return [{
    fn: 'createTable',
    params: [
      'cocktails',
      {
        id: {
          type: Sequelize.INTEGER,
          field: 'id',
          autoIncrement: true,
          primaryKey: true,
          allowNull: false,
        },
        cocktail_name: {
          type: Sequelize.STRING(255),
          field: 'cocktail_name',
          allowNull: false,
        },
        description: {
          type: Sequelize.STRING(255),
          field: 'description',
          allowNull: true,
        },
        image_path: {
          type: Sequelize.STRING(255),
          field: 'image_path',
          allowNull: true,
        },
      },
      {
        transaction,
      },
    ],
  },
  {
    fn: 'createTable',
    params: [
      'ingredients',
      {
        id: {
          type: Sequelize.INTEGER,
          field: 'id',
          autoIncrement: true,
          primaryKey: true,
          allowNull: false,
        },
        ingredient_name: {
          type: Sequelize.STRING(255),
          field: 'ingredient_name',
          allowNull: false,
        },
        type_id: {
          type: Sequelize.INTEGER,
          field: 'type_id',
          allowNull: false,
        },
        image_path: {
          type: Sequelize.STRING(255),
          field: 'image_path',
          allowNull: false,
        },
      },
      {
        transaction,
      },
    ],
  },
  {
    fn: 'createTable',
    params: [
      'roles',
      {
        id: {
          type: Sequelize.INTEGER,
          field: 'id',
          autoIncrement: true,
          primaryKey: true,
          allowNull: false,
        },
        name: {
          type: Sequelize.STRING(255),
          field: 'name',
          allowNull: false,
        },
      },
      {
        transaction,
      },
    ],
  },
  {
    fn: 'createTable',
    params: [
      'types',
      {
        id: {
          type: Sequelize.INTEGER,
          field: 'id',
          autoIncrement: true,
          primaryKey: true,
          allowNull: false,
        },
        type_name: {
          type: Sequelize.STRING(255),
          field: 'type_name',
          allowNull: false,
        },
      },
      {
        transaction,
      },
    ],
  },
  {
    fn: 'createTable',
    params: [
      'users',
      {
        id: {
          type: Sequelize.INTEGER,
          field: 'id',
          autoIncrement: true,
          primaryKey: true,
          allowNull: false,
        },
        username: {
          type: Sequelize.STRING(255),
          field: 'username',
          allowNull: false,
        },
        email: {
          type: Sequelize.STRING(255),
          field: 'email',
          allowNull: false,
        },
        password: {
          type: Sequelize.STRING(255),
          field: 'password',
          allowNull: false,
        },
      },
      {
        transaction,
      },
    ],
  },
  {
    fn: 'createTable',
    params: [
      'cocktail_ingredients',
      {
        id: {
          type: Sequelize.INTEGER,
          field: 'id',
          autoIncrement: true,
          primaryKey: true,
          allowNull: false,
        },
        cocktail_id: {
          type: Sequelize.INTEGER,
          onUpdate: 'CASCADE',
          onDelete: 'NO ACTION',
          field: 'cocktail_id',
          references: {
            model: 'cocktails',
            key: 'id',
          },
          allowNull: false,
        },
        ingredient_qty: {
          type: Sequelize.INTEGER,
          field: 'ingredient_qty',
          allowNull: false,
        },
        measurement_unit: {
          type: Sequelize.STRING(255),
          field: 'measurement_unit',
          allowNull: false,
        },
        ingredient_id: {
          type: Sequelize.INTEGER,
          onUpdate: 'CASCADE',
          onDelete: 'NO ACTION',
          field: 'ingredient_id',
          references: {
            model: 'ingredients',
            key: 'id',
          },
          allowNull: false,
        },
      },
      {
        transaction,
      },
    ],
  },
  {
    fn: 'createTable',
    params: [
      'user_roles',
      {
        createdAt: {
          type: Sequelize.DATE,
          field: 'createdAt',
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: 'updatedAt',
          allowNull: false,
        },
        roleId: {
          type: Sequelize.INTEGER,
          field: 'roleId',
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
          references: {
            model: {
              tableName: 'roles',
              table: 'roles',
              name: 'roles',
              schema: 'public',
              delimiter: '.',
            },
            key: 'id',
          },
          primaryKey: true,
        },
        userId: {
          type: Sequelize.INTEGER,
          field: 'userId',
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
          references: {
            model: {
              tableName: 'users',
              table: 'users',
              name: 'users',
              schema: 'public',
              delimiter: '.',
            },
            key: 'id',
          },
          primaryKey: true,
        },
      },
      {
        transaction,
      },
    ],
  },
  {
    fn: 'createTable',
    params: [
      'recipe',
      {
        id: {
          type: Sequelize.INTEGER,
          field: 'id',
          autoIncrement: true,
          primaryKey: true,
          allowNull: false,
        },
        cocktail_id: {
          type: Sequelize.INTEGER,
          field: 'cocktail_id',
          references: {
            model: 'cocktails',
            key: 'id',
          },
          allowNull: false,
        },
        step_decription: {
          type: Sequelize.STRING(255),
          field: 'step_decription',
          allowNull: false,
        },
      },
      {
        transaction,
      },
    ],
  },
  {
    fn: 'addIndex',
    params: [
      'cocktail_ingredients',
      [{
        name: 'id',
      }],
      {
        indexName: 'cocktail_ingredients_indexes',
        name: 'cocktail_ingredients_indexes',
        indicesType: 'UNIQUE',
        type: 'UNIQUE',
        transaction,
      },
    ],
  },
  {
    fn: 'addIndex',
    params: [
      'cocktails',
      [{
        name: 'id',
      }],
      {
        indexName: 'cocktails_indexes',
        name: 'cocktails_indexes',
        indicesType: 'UNIQUE',
        type: 'UNIQUE',
        transaction,
      },
    ],
  },
  {
    fn: 'addIndex',
    params: [
      'ingredients',
      [{
        name: 'id',
      }],
      {
        indexName: 'ingredients_indexes',
        name: 'ingredients_indexes',
        indicesType: 'UNIQUE',
        type: 'UNIQUE',
        transaction,
      },
    ],
  },
  {
    fn: 'addIndex',
    params: [
      'recipe',
      [{
        name: 'id',
      }],
      {
        indexName: 'recipe_indexes',
        name: 'recipe_indexes',
        indicesType: 'UNIQUE',
        type: 'UNIQUE',
        transaction,
      },
    ],
  },
  {
    fn: 'addIndex',
    params: [
      'roles',
      [{
        name: 'id',
      }],
      {
        indexName: 'roles_indexes',
        name: 'roles_indexes',
        indicesType: 'UNIQUE',
        type: 'UNIQUE',
        transaction,
      },
    ],
  },
  {
    fn: 'addIndex',
    params: [
      'types',
      [{
        name: 'id',
      }],
      {
        indexName: 'types_indexes',
        name: 'types_indexes',
        indicesType: 'UNIQUE',
        type: 'UNIQUE',
        transaction,
      },
    ],
  },
  {
    fn: 'addIndex',
    params: [
      'users',
      [{
        name: 'id',
      }],
      {
        indexName: 'users_indexes',
        name: 'users_indexes',
        indicesType: 'UNIQUE',
        type: 'UNIQUE',
        transaction,
      },
    ],
  },
  ]
}
const rollbackCommands = function (transaction) {
  return [{
    fn: 'dropTable',
    params: ['cocktail_ingredients', {
      transaction,
    }],
  },
  {
    fn: 'dropTable',
    params: ['cocktails', {
      transaction,
    }],
  },
  {
    fn: 'dropTable',
    params: ['ingredients', {
      transaction,
    }],
  },
  {
    fn: 'dropTable',
    params: ['user_roles', {
      transaction,
    }],
  },
  {
    fn: 'dropTable',
    params: ['recipe', {
      transaction,
    }],
  },
  {
    fn: 'dropTable',
    params: ['roles', {
      transaction,
    }],
  },
  {
    fn: 'dropTable',
    params: ['types', {
      transaction,
    }],
  },
  {
    fn: 'dropTable',
    params: ['users', {
      transaction,
    }],
  },
  ]
}

module.exports = {
  pos: 0,
  useTransaction: true,
  execute(queryInterface, Sequelize, _commands) {
    let index = this.pos
    function run(transaction) {
      const commands = _commands(transaction)
      return new Promise((resolve, reject) => {
        function next() {
          if (index < commands.length) {
            const command = commands[index]
            console.log(`[#${index}] execute: ${command.fn}`)
            index++
            queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject)
          } else resolve()
        }
        next()
      })
    }
    if (this.useTransaction) {
      return queryInterface.sequelize.transaction(run)
    }
    return run(null)
  },
  up(queryInterface, Sequelize) {
    return this.execute(queryInterface, Sequelize, migrationCommands)
  },
  down(queryInterface, Sequelize) {
    return this.execute(queryInterface, Sequelize, rollbackCommands)
  },
  info,
}
