const Sequelize = require('sequelize')

/**
 * Actions summary:
 *
 * changeColumn "roleId" on table "user_roles"
 * changeColumn "userId" on table "user_roles"
 * changeColumn "step_description" on table "recipe"
 *
 * */

const info = {
  revision: 3,
  name: 'noname',
  created: '2022-02-19T13:10:06.048Z',
  comment: '',
}

const migrationCommands = function (transaction) {
  return [
    {
      fn: 'changeColumn',
      params: [
        'recipe',
        'step_description',
        {
          type: Sequelize.TEXT,
          field: 'step_description',
          allowNull: false,
        },
        {
          transaction,
        },
      ],
    },
  ]
}
const rollbackCommands = function (transaction) {
  return [
    {
      fn: 'changeColumn',
      params: [
        'recipe',
        'step_description',
        {
          type: Sequelize.STRING(255),
          field: 'step_description',
          allowNull: false,
        },
        {
          transaction,
        },
      ],
    },
  ]
}

module.exports = {
  pos: 0,
  useTransaction: true,
  execute(queryInterface, Sequelize, _commands) {
    let index = this.pos
    function run(transaction) {
      const commands = _commands(transaction)
      return new Promise((resolve, reject) => {
        function next() {
          if (index < commands.length) {
            const command = commands[index]
            console.log(`[#${index}] execute: ${command.fn}`)
            index++
            queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject)
          } else resolve()
        }
        next()
      })
    }
    if (this.useTransaction) {
      return queryInterface.sequelize.transaction(run)
    }
    return run(null)
  },
  up(queryInterface, Sequelize) {
    return this.execute(queryInterface, Sequelize, migrationCommands)
  },
  down(queryInterface, Sequelize) {
    return this.execute(queryInterface, Sequelize, rollbackCommands)
  },
  info,
}
