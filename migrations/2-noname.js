const Sequelize = require('sequelize')

/**
 * Actions summary:
 *
 * removeColumn "step_decription" from table "recipe"
 * addColumn "step_description" to table "recipe"
 * changeColumn "userId" on table "user_roles"
 * changeColumn "roleId" on table "user_roles"
 *
 * */

const info = {
  revision: 2,
  name: 'noname',
  created: '2022-02-19T13:06:01.620Z',
  comment: '',
}

const migrationCommands = function (transaction) {
  return [{
    fn: 'removeColumn',
    params: [
      'recipe',
      'step_decription',
      {
        transaction,
      },
    ],
  },
  {
    fn: 'addColumn',
    params: [
      'recipe',
      'step_description',
      {
        type: Sequelize.STRING(255),
        field: 'step_description',
        allowNull: false,
      },
      {
        transaction,
      },
    ],
  },
  ]
}
const rollbackCommands = function (transaction) {
  return [{
    fn: 'removeColumn',
    params: [
      'recipe',
      'step_description',
      {
        transaction,
      },
    ],
  },
  {
    fn: 'addColumn',
    params: [
      'recipe',
      'step_decription',
      {
        type: Sequelize.STRING(255),
        field: 'step_decription',
        allowNull: false,
      },
      {
        transaction,
      },
    ],
  },
  ]
}

module.exports = {
  pos: 0,
  useTransaction: true,
  execute(queryInterface, Sequelize, _commands) {
    let index = this.pos
    function run(transaction) {
      const commands = _commands(transaction)
      return new Promise((resolve, reject) => {
        function next() {
          if (index < commands.length) {
            const command = commands[index]
            console.log(`[#${index}] execute: ${command.fn}`)
            index++
            queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject)
          } else resolve()
        }
        next()
      })
    }
    if (this.useTransaction) {
      return queryInterface.sequelize.transaction(run)
    }
    return run(null)
  },
  up(queryInterface, Sequelize) {
    return this.execute(queryInterface, Sequelize, migrationCommands)
  },
  down(queryInterface, Sequelize) {
    return this.execute(queryInterface, Sequelize, rollbackCommands)
  },
  info,
}
