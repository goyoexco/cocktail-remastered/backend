const controllerIngredient = require('../controllers/ingredient')

module.exports = function (app) {
  app.use((req, res, next) => {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept',
    )
    next()
  })

  app.get(
    '/api/ingredient/all',
    [
      // authJwt.verifyToken
    ],
    controllerIngredient.getIngredients,
  )
}
