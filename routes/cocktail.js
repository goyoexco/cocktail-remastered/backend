const controllerCocktail = require('../controllers/cocktail')
const controllervCocktailIngredients = require('../controllers/cocktail_ingredients')
const controllerRecipe = require('../controllers/recipe')

module.exports = function (app) {
  app.use((req, res, next) => {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept',
    )
    next()
  })

  app.get(
    '/api/cocktail/all',
    [
      // authJwt.verifyToken
    ],
    controllerCocktail.getCocktails,
  )

  app.post(
    '/api/cocktail/addCocktail',
    [
      // authJwt.verifyToken, authJwt.isModerator, verifyCocktails.checkDuplicateCocktail,
    ],
    controllerCocktail.addCocktail,
  )

  app.post(
    '/api/cocktail/addCocktailIngredients',
    [
      // authJwt.verifyToken, authJwt.isModerator,
    ],
    controllervCocktailIngredients.addCocktailIngredient,
  )

  app.post(
    '/api/cocktail/addRecipe',
    [
      // authJwt.verifyToken, authJwt.isModerator,
    ],
    controllerRecipe.addRecipe,
  )

  app.get(
    '/api/cocktail_ingredients/:id',
    [
      // authJwt.verifyToken,
    ],
    controllervCocktailIngredients.getCocktailIngredients,
  )

  app.get(
    '/api/cocktail_recipe/:id',
    [
      // authJwt.verifyToken,
    ],
    controllerRecipe.getCocktailRecipe,
  )

  app.post(
    '/api/cocktailForIngredients',
    [
      // authJwt.verifyToken,
    ],
    controllerCocktail.getCocktailsWithIngredients,
  )
}
