const sequelize = require('../config/sequelize')
const initModels = require('../models/init-models')

const models = initModels(sequelize)
const db = require('../models')

const { ROLES } = db

const checkDuplicateUsernameOrEmail = (req, res, next) => {
  // Username
  models.users
    .findOne({
      where: {
        username: req.body.username,
      },
    })
    .then((user) => {
      if (user) {
        res.status(400).send({
          message: 'Failed! Username is already in use!',
        })
        return
      }

      // Email
      models.users
        .findOne({
          where: {
            email: req.body.email,
          },
        })
        .then((user2) => {
          if (user2) {
            res.status(400).send({
              message: 'Failed! Email is already in use!',
            })
            return
          }

          next()
        })
    })
}

const checkRolesExisted = (req, res, next) => {
  if (req.body.roles) {
    for (let i = 0; i < req.body.roles.length; i++) {
      if (!ROLES.includes(req.body.roles[i])) {
        res.status(400).send({
          message: `Failed! Role does not exist = ${req.body.roles[i]}`,
        })
        return
      }
    }
  }

  next()
}

const verifySignUp = {
  checkDuplicateUsernameOrEmail,
  checkRolesExisted,
}

module.exports = verifySignUp
