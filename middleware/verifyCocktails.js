const { Op } = require('sequelize')
const sequelize = require('../config/sequelize')
const initModels = require('../models/init-models')

const models = initModels(sequelize)

const checkDuplicateCocktail = (req, res, next) => {
  // Cocktail Name case incensitive
  models.cocktails
    .findOne({
      where: {
        cocktail_name: {
          [Op.iLike]: `%${req.body.cocktail_name}%`,
        },
      },
    })
    .then((cocktail) => {
      if (cocktail) {
        res.status(400).send({
          message: 'Failed! Cocktail is already in the database!',
        })
        return
      }

      next()
    })
}

const verifyCocktails = {
  checkDuplicateCocktail,
}

module.exports = verifyCocktails
