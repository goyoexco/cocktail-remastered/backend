const { Sequelize } = require('sequelize')

const sequelize = require('../config/sequelize')

const initModels = require('../models/init-models')

const models = initModels(sequelize)

/**
 * select * from (select COUNT(cocktail_id) as counted, cocktail_id from cocktail_ingredients
where ingredient_id =1 or ingredient_id = 24
group by cocktail_id) as test
join cocktails on test.cocktail_id= cocktails.id
where counted = 2
 */

exports.getCocktails = (req, res) => {
  let filter = {}
  const { name } = req.query
  if (name) {
    filter = {
      where: {
        cocktail_name: {
          [Sequelize.Op.iLike]: `%${name}%`,
        },
      },
    }
  }
  models.cocktails.findAll(filter).then((cocktail) => {
    if (cocktail) {
      res.json(cocktail)
    } else {
      res.status(404).send()
    }
  })
}

exports.addCocktail = async (req, res) => {
  // Save Cocktail to Database
  models.cocktails
    .create({
      cocktail_name: req.body.cocktail_name,
      description: req.body.description,
      image_path: req.body.image_path,
    }).then(() =>
      res.status(200).send({ message: 'Cocktail added successfully' }))
    .catch((err) => {
      res.status(500).send({ message: err.message })
    })
}

exports.getCocktailsWithIngredients = (req, res) => {
  let filter = {}

  const ingredientArray = req.body.ingredient_id.map(
    (
      ingredient_id,
    ) =>
      ({
        ingredient_id,
      }),
  )
  filter = {

    where: {
      [Sequelize.Op.or]: ingredientArray,
    },
    include: [{ model: models.cocktails, as: 'cocktail' }],
    group: ['cocktail_id', 'cocktail.cocktail_name', 'cocktail.description', 'cocktail.image_path'],
  }
  models.cocktail_ingredients.count(filter).then((cocktail) => {
    if (cocktail.length !== 0) {
      const final_cocktail = []
      cocktail.forEach((item) => {
        if (item.count === ingredientArray.length) {
          final_cocktail.push(item)
        }
      })
      if (final_cocktail.length > 0) {
        res.json(final_cocktail)
      } else {
        res.status(404).send({ message: 'No cocktails have all these ingredients !' })
      }
    } else {
      res.status(404).send({ message: 'Select some ingredients !' })
    }
  })
}
