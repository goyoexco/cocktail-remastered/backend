const sequelize = require('../config/sequelize')

const initModels = require('../models/init-models')

const models = initModels(sequelize)

exports.addCocktailIngredient = async (req, res) => {
  // Maybe Create a cocktail if not present ?
  const { cocktail_id } = req.body

  for (let i = 0; i < req.body.ingredient_cocktail.length; i++) {
    const { ingredient_name } = req.body.ingredient_cocktail[i]
    const { image_path } = req.body.ingredient_cocktail[i]
    const { ingredient_qty } = req.body.ingredient_cocktail[i]
    const { measurement_unit } = req.body.ingredient_cocktail[i]
    models.ingredients.findOrCreate({
      raw: true,
      where: {
        ingredient_name,
        type_id: 1,
        image_path,
      },
    }).then((results) => {
      models.cocktail_ingredients.findOrCreate({
        where: {
          cocktail_id,
          ingredient_qty,
          measurement_unit,
          ingredient_id: results[0].id,
        },
      })
    })
  }
  res.send('Done')
}

exports.getCocktailIngredients = (req, res) => {
  const { id } = req.params
  models.cocktail_ingredients.findAll({
    include: [
      'ingredient',
    ],
    where: {
      cocktail_id: id,
    },
  }).then((cocktail_ingredients) => {
    if (cocktail_ingredients) {
      res.json(cocktail_ingredients)
    } else {
      res.status(404).send()
    }
  })
}
