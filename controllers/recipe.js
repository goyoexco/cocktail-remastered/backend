const sequelize = require('../config/sequelize')

const initModels = require('../models/init-models')

const models = initModels(sequelize)

exports.addRecipe = async (req, res) => {
  const { cocktail_id } = req.body

  for (let i = 0; i < req.body.recipe_cocktail.length; i++) {
    const { step_description } = req.body.recipe_cocktail[i]
    models.recipe.create({
      cocktail_id,
      step_description,
    })
  }

  res.json('Done')
}

exports.getCocktailRecipe = (req, res) => {
  const { id } = req.params
  models.recipe.findAll({
    raw: true,
    where: {
      cocktail_id: id,
    },
  }).then((cocktail_recipe) => {
    if (cocktail_recipe) {
      res.json(cocktail_recipe)
    } else {
      res.status(404).send()
    }
  })
}
