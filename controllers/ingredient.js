const { Sequelize } = require('sequelize')

const sequelize = require('../config/sequelize')

const initModels = require('../models/init-models')

const models = initModels(sequelize)

exports.getIngredients = (req, res) => {
  let filter = {
    attributes: [
      'id',
      'ingredient_name',
      'image_path',
    ],
    include: [
      'type',
    ],
  }
  const { name } = req.query
  if (name) {
    filter = {
      attributes: [
        'id',
        'ingredient_name',
        'image_path',
      ],
      include: [
        'type',
      ],
      where: {
        ingredient_name: {
          [Sequelize.Op.iLike]: `%${name}%`,
        },
      },
    }
  }
  models.ingredients.findAll(filter).then((ingredient) => {
    if (ingredient) {
      res.json(ingredient)
    } else {
      res.status(404).send()
    }
  })
}
