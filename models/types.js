const Sequelize = require('sequelize')

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('types', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    type_name: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
  }, {
    sequelize,
    tableName: 'types',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: 'types_indexes',
        unique: true,
        fields: [
          { name: 'id' },
        ],
      },
    ],
  })
}
