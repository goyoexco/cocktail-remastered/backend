const Sequelize = require('sequelize')

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('recipe', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    cocktail_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'cocktails',
        key: 'id',
      },
    },
    step_description: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
  }, {
    sequelize,
    tableName: 'recipe',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: 'recipe_indexes',
        unique: true,
        fields: [
          { name: 'id' },
        ],
      },
    ],
  })
}
