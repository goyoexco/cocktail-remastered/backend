module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    'ingredients',
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      ingredient_name: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      type_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'types',
          key: 'id',
        },
      },
      image_path: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
    },
    {
      sequelize,
      tableName: 'ingredients',
      schema: 'public',
      timestamps: false,
      indexes: [
        {
          name: 'ingredients_indexes',
          unique: true,
          fields: [{ name: 'id' }],
        },
      ],
    },
  )
}
