const { DataTypes } = require('sequelize')
const _cocktail_ingredients = require('./cocktail_ingredients')
const _cocktails = require('./cocktails')
const _ingredients = require('./ingredients')
const _recipe = require('./recipe')
const _types = require('./types')
const _users = require('./users')
const _roles = require('./roles')

function initModels(sequelize) {
  const cocktail_ingredients = _cocktail_ingredients(sequelize, DataTypes)
  const cocktails = _cocktails(sequelize, DataTypes)
  const ingredients = _ingredients(sequelize, DataTypes)
  const recipe = _recipe(sequelize, DataTypes)
  const types = _types(sequelize, DataTypes)
  const users = _users(sequelize, DataTypes)
  const roles = _roles(sequelize, DataTypes)

  cocktail_ingredients.belongsTo(cocktails, {
    as: 'cocktail',
    foreignKey: 'cocktail_id',
  })
  cocktails.hasMany(cocktail_ingredients, {
    as: 'cocktail_ingredients',
    foreignKey: 'cocktail_id',
  })
  recipe.belongsTo(cocktails, { as: 'cocktail', foreignKey: 'cocktail_id' })
  cocktails.hasMany(recipe, { as: 'recipes', foreignKey: 'cocktail_id' })
  cocktail_ingredients.belongsTo(ingredients, {
    as: 'ingredient',
    foreignKey: 'ingredient_id',
  })
  ingredients.hasMany(cocktail_ingredients, {
    as: 'cocktail_ingredients',
    foreignKey: 'ingredient_id',
  })
  ingredients.belongsTo(types, { as: 'type', foreignKey: 'type_id' })
  types.hasMany(ingredients, { as: 'ingredients', foreignKey: 'type_id' })
  roles.belongsToMany(users, {
    through: 'user_roles',
    foreignKey: 'roleId',
    otherKey: 'userId',
  })
  users.belongsToMany(roles, {
    through: 'user_roles',
    foreignKey: 'userId',
    otherKey: 'roleId',
  })

  return {
    cocktail_ingredients,
    cocktails,
    ingredients,
    recipe,
    types,
    users,
    roles,
  }
}
module.exports = initModels
module.exports.initModels = initModels
module.exports.default = initModels
