module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    'cocktails',
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      cocktail_name: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      description: {
        type: DataTypes.STRING(255),
        allowNull: true,
      },
      image_path: {
        type: DataTypes.STRING(255),
        allowNull: true,
      },
    },
    {
      sequelize,
      tableName: 'cocktails',
      schema: 'public',
      timestamps: false,
      indexes: [
        {
          name: 'cocktails_indexes',
          unique: true,
          fields: [{ name: 'id' }],
        },
      ],
    },
  )
}
