module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    'cocktail_ingredients',
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      cocktail_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'cocktails',
          key: 'id',
        },
      },
      ingredient_qty: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      measurement_unit: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      ingredient_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'ingredients',
          key: 'id',
        },
      },
    },
    {
      sequelize,
      tableName: 'cocktail_ingredients',
      schema: 'public',
      timestamps: false,
      indexes: [
        {
          name: 'cocktail_ingredients_indexes',
          unique: true,
          fields: [{ name: 'id' }],
        },
      ],
    },
  )
}
